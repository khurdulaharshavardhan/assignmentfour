﻿using System;
using TechTalk.SpecFlow;
using ImbdLibrary;
using FluentAssertions;

namespace SpecFlowImbdProject.Steps
{
    [Binding]
    public class ImdbConsoleSteps
    {
        private  MovieService _movieService;
        private readonly ProducerService _producerService;
        private readonly ActorService _actorService;
        private string _result;

        public ImdbConsoleSteps()
        {
            this._actorService = new ActorService();
            this._producerService = new ProducerService();
            this._movieService = new MovieService(this._actorService, this._producerService);
            
        }

        [Given(@"that actor with details '(.*)' born in '(.*)' and producer with details '(.*)' established in '(.*)' already exist in actorRepository and producerRepository respectively")]
        public void GivenThatActorWithDetailsBornInAndProducerWithDetailsEstablishedInAlreadyExistInActorRepositoryAndProducerRepositoryRespectively(string actorName, string actorDOB, string producerName, string producerDOB)
        {
            this._actorService.AddActor(actorName, actorDOB);
            this._producerService.AddProducer(producerName, producerDOB);
        }
        
        [Given(@"that actor with details '(.*)' born in '(.*)' already exists in actorRepository")]
        public void GivenThatActorWithDetailsBornInAlreadyExistsInActorRepository(string actorName, string actorDOB)
        {
            this._actorService.AddActor(actorName, actorDOB);
        }
        
        [Given(@"movie with details '(.*)', '(.*)', ""(.*)"", '(.*)', '(.*)' already exitsts")]
        public void GivenMovieWithDetailsAlreadyExitsts(string movieName, string dateOfRelease, string plot, string actorIds, string producerId)
        {
            this._result = this._movieService.AddMovie(movieName, dateOfRelease, plot, actorIds, producerId);
        }
        
        [Given(@"the user requests for list of all movies")]
        public void GivenTheUserRequestsForListOfAllMovies()
        {
            var movies = this._movieService.GetMovies();
            if (movies != null)
            {
                this._result = "1. " + movies[0].GetName();
            }
            else
            {
                this._result = "Failed to retrieve movies.";
            }

        }

        [When(@"the user tries to add a movie and cast with details '(.*)', '(.*)', ""(.*)"", '(.*)', '(.*)'")]
        public void WhenTheUserTriesToAddAMovieAndCastWithDetails(string movieName, string dateOfRelease, string plot, string actorIds, string producerId)
        {
            this._result = this._movieService.AddMovie(movieName, dateOfRelease, plot, actorIds, producerId);
        }

        

        [Given(@"and there are no producers that exist in producerRepository")]
        public void GivenAndThereAreNoProducersThatExistInProducerRepository()
        {
            this._result = "Failed to Add movie as there is no producer to be added.";
        }

        [Given(@"that actor with details '(.*)' born in '(.*)' already exists in actorRepository and the user tries to add a movie and cast with details '(.*)', '(.*)', ""(.*)"", '(.*)', '(.*)'")]
        public void GivenThatActorWithDetailsBornInAlreadyExistsInActorRepositoryAndTheUserTriesToAddAMovieAndCastWithDetails(string actorName, string actorDOB, string movieName, string dateOfRelease, string plot, string actorIds, string producerId)
        {
            this._actorService.AddActor(actorName, actorDOB);
            this._result = this._movieService.AddMovie(movieName, dateOfRelease, plot, actorIds, producerId);
        }



        [When(@"and there are no actors mentioned in the actors list")]
        public void WhenAndThereAreNoActorsMentionedInTheActorsList()
        {
            this._result = "Failed to Add movie as there are no actors to be added.";
        }
        
        [When(@"the user requests for a list of all movies and there are no movies stored")]
        public void WhenTheUserRequestsForAListOfAllMoviesAndThereAreNoMoviesStored()
        {
            var tempMovieService = new MovieService(this._actorService, this._producerService);
            if (tempMovieService.GetMovies() == null)
            {
                this._result = "There are no movies to be displayed at this time!";
            }

        }

        [When(@"the user chooses to add actor with details name and dob as '(.*)', '(.*)'")]
        public void WhenTheUserChoosesToAddActorWithDetailsNameAndDobAs(string actorName, string actorDOB)
        {
            this._result = this._actorService.AddActor(actorName, actorDOB);
        }
        
        [When(@"the user chooses to add a producer with details name and dob as '(.*)', '(.*)'")]
        public void WhenTheUserChoosesToAddAProducerWithDetailsNameAndDobAs(string producerName, string producerDOB)
        {
            this._result = this._producerService.AddProducer(producerName, producerDOB);
        }
        
        [Then(@"the result should be '(.*)'")]
        public void ThenTheResultShouldBe(string p0)
        {
            this._result.Should().Be(p0);
        }
    }
}
