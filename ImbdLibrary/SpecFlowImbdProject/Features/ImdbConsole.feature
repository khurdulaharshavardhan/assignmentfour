﻿Feature: ImdbConsole
		this feautre must allow a user to do this.
		the following are carried out by IMDBConsoleApplication.

Background: I am the user.


@addMovie
Scenario: add Movie - Success
	Given that actor with details 'pewdiepie' born in '1992' and producer with details 'T-series' established in '1990' already exist in actorRepository and producerRepository respectively
	When the user tries to add a movie and cast with details 'Pewdiepie and his Adventures', '28-02-2022', "A swedish youtuber, with a loose mouth tries to become the world's most subscribed youtube channel, will he be able to conquer youtube?", '1', '1'
	Then the result should be 'Movie added, successfully!'

@addMovie
Scenario: add Movie - Failure - Empty actors list.
	When the user tries to add a movie and cast with details 'Pewdiepie and his Adventures', '28-02-2022', "A swedish youtuber, with a loose mouth tries to become the world's most subscribed youtube channel, will he be able to conquer youtube?", '1', '1'
	And and there are no actors mentioned in the actors list
	Then the result should be 'Failed to Add movie as there are no actors to be added.'

@addMovie
Scenario: add Movie - Failure - Empty producer list.
	Given that actor with details 'pewdiepie' born in '1992' already exists in actorRepository and the user tries to add a movie and cast with details 'Pewdiepie and his Adventures', '28-02-2022', "A swedish youtuber, with a loose mouth tries to become the world's most subscribed youtube channel, will he be able to conquer youtube?", '1', '1'
	And and there are no producers that exist in producerRepository
	Then the result should be 'Failed to Add movie as there is no producer to be added.'


@listMovie
Scenario: list Movies - Success
	Given movie with details 'Pewdiepie and his Adventures', '28-02-2022', "A swedish youtuber, with a loose mouth tries to become the world's most subscribed youtube channel, will he be able to conquer youtube?", '1', '1' already exitsts
	And the user requests for list of all movies
	Then the result should be '1. Pewdiepie and his Adventures'

@listMovie
Scenario: list Movies - Failure - There are no Movies stored
	When the user requests for a list of all movies and there are no movies stored
	Then the result should be 'There are no movies to be displayed at this time!'


@addActor
Scenario: add Actor - Success
	When the user chooses to add actor with details name and dob as 'Pewdiepie', '1992'
	Then the result should be 'Actor details have been stored successfully!'

@addActor
Scenario: add Actor - Failure - Invalid name
	When the user chooses to add actor with details name and dob as '', '1992'
	Then the result should be 'Invalid Input!'

@addActor
Scenario: add Actor - Failure - Invalid actor date of birth
	When the user chooses to add actor with details name and dob as 'Pewdiepie', ''
	Then the result should be 'Invalid Input!'

@addProducer
Scenario: add Producer - Success
	When the user chooses to add a producer with details name and dob as 'T-series', '1990'
	Then the result should be 'Producer details have been stored successfully!'

@addProducer
Scenario: add Producer - Failure - Invalid producer name
	When the user chooses to add a producer with details name and dob as '', '1990'
	Then the result should be 'Invalid Input!'

@addProducer
Scenario: add Producer - Failure - Invalid producer date of birth
	When the user chooses to add a producer with details name and dob as 'T-series', ''
	Then the result should be 'Invalid Input!'