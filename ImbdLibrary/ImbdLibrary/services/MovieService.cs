﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ImbdLibrary
{
    public class MovieService
    {
        private readonly MovieRepository _movieRepository;
        private readonly ActorService _actorService;
        private readonly ProducerService _producerService;

        public MovieService(ActorService actorService, ProducerService producerService)
        {
            this._actorService = actorService;
            this._producerService = producerService;
            this._movieRepository = new MovieRepository(actorService, producerService);
            
        }

        public int GetTotalMovies()
        {
            return this._movieRepository.GetTotalMovies();
        }

        bool IsInvalid(string str)
        {
            return (str == null || str == string.Empty);
        }

        
        public string AddMovie(string movieName, string date, string plot,string actorIds, string producerId)
        {
            
            if (IsInvalid(movieName) || IsInvalid(date) || IsInvalid(plot))
            {
                return "Invalid Input!";
            }
            else
            {

                this._movieRepository.AddMovie(movieName, plot, date);
                if (IsInvalid(actorIds))
                    {
                       return "Invalid Input!";
                    }
                else
                    {
                       if(!this._movieRepository.AddActors(actorIds))
                       return "Invalid Input!";
                    }
                
                if (IsInvalid(producerId))
                    {
                        return "Invalid Input!";
                    }
                else
                    {
                        var index = int.Parse(producerId);
                        var producers = this._producerService.GetProducers();
                        var count = this._producerService.GetTotalProducers();
                    if (index > 0 && index <= count)
                    {
                        this._movieRepository.SetProducer(producers[index - 1].GetName());
                    }
                    else
                        return "Invalid Input!";
                    }
                }
                return "Movie added, successfully!";
            }
        

        public void AddProducer(int givenId)
        {
            var index = givenId;

            if (index > 0 && index <= this._producerService.GetProducers().Count)
            {
                this._movieRepository.SetProducer(this._producerService.GetProducers()[index - 1].GetName());
            }
        }

        

        public string DeleteMovie(int x)
        {
            

            if(x>0 && x <= this._movieRepository.GetTotalMovies())
            {
                

                this._movieRepository.DeleteMovie(x);
                return "Movie has been deleted successfully!";
            }
            else
            {
                return "Invalid Input!";
            }
        }

        public List<Movie> GetMovies()
        {
            if (this._movieRepository.GetTotalMovies() == 0)
                return null;
            else
                return this._movieRepository.GetMovies();
        }

        public void DisplayMovies()
        {
            if (this._movieRepository.GetTotalMovies() == 0)
            {
                Console.WriteLine("There are no movies to be displayed at this time!");
            }
            else
            {
                for(int i=0;i< this._movieRepository.GetTotalMovies(); i++)
                {
                    Console.WriteLine("{0}. {1}", (i + 1), this._movieRepository.GetMovies()[i].GetName());
                }
            }
        }

        public void ViewMovieDetails(int movieId)
        {
            if (movieId>0 && movieId <= this._movieRepository.GetTotalMovies())
            {
                movieId--;
                var movies = this._movieRepository.GetMovies();
                Console.WriteLine("Movie Title: {0}", movies[movieId].GetName());
                Console.WriteLine("Plot of the film: {0}", movies[movieId].GetPlot());
                Console.WriteLine("Year of release: {0}", movies[movieId].GetYearOfRelease());
                Console.Write("CAST:");
                var cast = movies[movieId].GetActors();
                
                if(cast == null)
                {
                    Console.WriteLine("There's no cast added to this movie!");
                }
                else
                {
                    for(int i = 0; i < cast.Count; i++)
                    {
                        Console.Write(" {0}", cast[i].GetName());
                    }
                }
                Console.WriteLine("\nProduced by: {0}", movies[movieId].GetProducer());
            }
            else
            {
                Console.WriteLine("Invalid Movie Id!");
            }
        }
    }
}
