﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ImbdLibrary
{
    public class ProducerService 
    {
        private readonly ProducerRepository _producerRepository;

        public ProducerService()
        {
            this._producerRepository = new ProducerRepository();
        }

        public int GetTotalProducers()
        {
            return this._producerRepository.GetTotalProducers();
        }

        public string AddProducer(string producerName, string producerDOB)
        {
            if (producerName == null || producerName == string.Empty || producerDOB == null || producerDOB == string.Empty)
            {
                return "Invalid Input!";
            }
            else
            {
                return this._producerRepository.AddProducer(producerName, producerDOB);
            }
        }

        

        public List<Producer> GetProducers()
        {
            if (this._producerRepository.GetTotalProducers() == 0)
            {
                return null;
            }
            else
            {
                return this._producerRepository.ListProducers();
            }
        }

        public void DisplayProducers()
        {
            if (this._producerRepository.GetTotalProducers() == 0)
            {
                Console.WriteLine("There are no Producers that can be added to the movie!");
            }
            else
            {
                Console.WriteLine("Here's a list of All producers: ");

                for (int i = 0; i < this._producerRepository.GetTotalProducers(); i++)
                {
                    Console.WriteLine("{0}. {1}", (i + 1), this._producerRepository.ListProducers()[i].GetName());
                }
            }

        }
    }
}
