﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ImbdLibrary
{
    public class ImdbService
    {
        private readonly MovieService _movieService;
        private readonly ActorService _actorService;
        private readonly ProducerService _producerService;

        public ImdbService()
        {
            this._actorService = new ActorService();
            this._producerService = new ProducerService();
            this._movieService = new MovieService(this._actorService, this._producerService);
        }



        public void DisplayMovies()
        {

            this._movieService.DisplayMovies();
            if (this._movieService.GetTotalMovies() != 0)
            {
                Console.WriteLine("Enter the movie Id to view more details: ");
                try
                {
                    var movieId = Convert.ToInt32(Console.ReadLine());
                    this._movieService.ViewMovieDetails(movieId);
                }
                catch (Exception)
                {
                    Console.WriteLine("Invalid Input!");
                }
            }
        }

        public string AddMovie() {
            Console.WriteLine("Please enter the Title of the film: ");
            var movieName = Console.ReadLine();

            Console.WriteLine("Please enter the year of release of the film: ");
            var date = Console.ReadLine();

            Console.WriteLine("Please enter the Plot of the film: ");
            var plot = Console.ReadLine();
            var actorIds = "";
            var producerId = "";
            if (this._actorService.GetTotalActors() != 0)
            {
                this._actorService.DisplayActors();
                Console.WriteLine("Please enter actor IDs seperated by space to add them to cast of {0}", movieName);
                 actorIds = Console.ReadLine();
            }
            else
            {
                return "There are no actors that can be added right now!";
            }

            if (this._producerService.GetTotalProducers() != 0)
            {
                this._producerService.DisplayProducers();
                var producers = this._producerService.GetProducers();
                Console.WriteLine("Please enter producer ID who produced {0}", movieName);
                 producerId = Console.ReadLine();
            }
            else
            {
                return "There are no producers that can be added right now!";
            }

            return this._movieService.AddMovie(movieName,date,plot,actorIds,producerId);
        }


        public string AddActor()
        {
            Console.Write("[ACTOR INPUT] Enter the name of the actor to be added: ");
            var actorName = Console.ReadLine();

            Console.Write("[ACTOR INPUT] Enter the Date of Birth of the actor to be added: ");
            var actorDOB = Console.ReadLine();
            return this._actorService.AddActor(actorName, actorDOB);
        }


        public string AddProducer() {

            Console.Write("[PRODUCER INPUT] Enter the name of the Producer to be added: ");
            var producerName = Console.ReadLine();

            Console.Write("[PRODUCER INPUT] Enter the Date of Birth of the Producer to be added: ");
            var producerDOB = Console.ReadLine();
            return this._producerService.AddProducer(producerName, producerDOB);
        }


        public string DeleteMovie()
        {
            this._movieService.DisplayMovies();
            Console.WriteLine("Please enter the movie id that has to be deleted: ");
            int x;
            try
            {
                x = Convert.ToInt32(Console.ReadLine());
            }
            catch (Exception)
            {
                return "Invalid Input!";
            }

            return this._movieService.DeleteMovie(x);
        }
    }
}
