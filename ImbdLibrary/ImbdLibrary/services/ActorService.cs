﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ImbdLibrary
{
    public class ActorService 
    {
        private readonly ActorRepository _actorRepository;
        public ActorService()
        {
            this._actorRepository = new ActorRepository();
        }

        public string AddActor(string actorName, string actorDOB)
        {
            if(actorName == null || actorName == string.Empty || actorDOB == null || actorDOB == string.Empty)
            {
                return "Invalid Input!";
            }
            else
            {
                return this._actorRepository.AddActor(actorName, actorDOB);
            }
        }


        public int GetTotalActors()
        {
            return this._actorRepository.GetTotalActors();
        }


        public List<Actor> GetActors()
        {
            return this._actorRepository.GetActors();
        }

              

        public void DisplayActors()
        {
            if(this._actorRepository.GetTotalActors() == 0) {
                Console.WriteLine("There are no actors that can be added to the movie!");
            }
            else
            {
                Console.WriteLine("Here's a list of All actors: ");

                for(int i = 0; i < this._actorRepository.GetTotalActors(); i++)
                {
                    Console.WriteLine("{0}. {1}", (i + 1), this._actorRepository.GetActors()[i].GetName());
                }
            }
            
        }
    }
}
