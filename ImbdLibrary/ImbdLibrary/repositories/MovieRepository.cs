﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ImbdLibrary
{
    public class MovieRepository : IMovieRepository
    {
        private Movie _movie;
        private readonly List<Movie> _movies;
        private readonly ActorService _actorService;
        private readonly ProducerService _producerService;
        private int _totalMovies;


        public MovieRepository(ActorService actorService, ProducerService producerService)
        {
            this._movies = new List<Movie>();
            this._actorService = actorService;
            this._producerService = producerService;
            this._totalMovies = 0;
            this._movie = new Movie();
            
        }

        public int GetTotalMovies()
        {
            return this._totalMovies;
        }

        public void AddMovie(string movieName, string plot, string date)
        {
            this._movie = new Movie();
            this._movie.SetName(movieName);
            this._movie.SetPlot(plot);
            this._movie.SetYearOfRelease(date);
            this._movie.SetId(this.GetTotalMovies() + 1);
            this._movies.Add(this._movie);
            this._totalMovies++;
            
        }

        public void DeleteMovie(int x)
        {
            this._movies.RemoveAt(x - 1);
            this._totalMovies--;
        }



        public void SetProducer(string producer)
        {
            this._movie.SetProducer(producer);
        }


        public List<Movie> GetMovies()
        {
            if (this.GetTotalMovies() == 0)
            {
                return null;
            }
            else
            {
                return this._movies;
            }
        }

        public bool AddActors(string givenIds)
        {
            string[] ids = givenIds.Trim().Split(" ");

            foreach (string id in ids)
            {
                var index = int.Parse(id);

                var actors = this._actorService.GetActors();

                if (actors != null && (index > 0 && index <= actors.Count))
                    this._movie.UpdateCast(actors[index - 1]);
                else
                    return false;
            }
            return true;
        }
        
        public void UpdateCast(int actorId)
        {
            var index = actorId;
            var actors = this._actorService.GetActors();

            if (actors != null && (index > 0 && index <= actors.Count))
                this._movie.UpdateCast(actors[index - 1]);
        }
    }
}
