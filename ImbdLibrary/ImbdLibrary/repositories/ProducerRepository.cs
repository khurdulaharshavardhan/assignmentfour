﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ImbdLibrary
{
    class ProducerRepository : IProducerRepository
    {

        private readonly Producer _producer;
        private readonly List<Producer> _producers;
        private int _totalProducers;

        public ProducerRepository()
        {
            this._producer = new Producer();
            this._producers = new List<Producer>();
            this._totalProducers = 0;
        }
        public string AddProducer(string producerName, string producerDOB)
        {
            var tempProducer = new Producer();
            tempProducer.SetName(producerName);
            tempProducer.SetDOB(producerDOB);
            tempProducer.SetId(this.GetTotalProducers() + 1);
            this.UpdateProducerList(tempProducer);
            this._totalProducers++;
            return "Producer details have been stored successfully!";
        }

        void UpdateProducerList(Producer producer)
        {
            this._producers.Add(producer);
        }

        public List<Producer> ListProducers()
        {
            if (this._totalProducers == 0)
            {
                return null;
            }
            else
            {
                return this._producers;
            }
        }

        public int GetTotalProducers()
        {
            return this._totalProducers;
        }
    }
}
