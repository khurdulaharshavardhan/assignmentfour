﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ImbdLibrary
{
    public interface IProducerRepository
    {
        string AddProducer(string producerName, string producerDOB);
        List<Producer> ListProducers();
    }
}
