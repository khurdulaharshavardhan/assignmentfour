﻿using System;
using System.Collections.Generic;

namespace ImbdLibrary
{
    public interface IMovieRepository
    {
        void AddMovie(string movieName, string plot, string date);

        List<Movie> GetMovies();

        void DeleteMovie(int x);

    }
}
