﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ImbdLibrary
{
    public interface IActorRepository
    {
        string AddActor(string actorName, string actorDOB);
        List<Actor> GetActors();
    }
}
