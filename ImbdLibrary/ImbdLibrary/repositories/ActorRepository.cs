﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ImbdLibrary
{
    public class ActorRepository : IActorRepository
    {
        private readonly List<Actor> _actors;
        private int _totalActors;

        public ActorRepository()
        {
            this._actors = new List<Actor>();  
            this._totalActors = 0;
        }

        public string AddActor(string actorName, string actorDOB)
        {
            var tempActor = new Actor();

            tempActor.SetName(actorName);
            tempActor.SetDOB(actorDOB);
            tempActor.SetId(this.GetTotalActors() + 1);
            this.UpdateActorList(tempActor);
            this._totalActors++;
            return "Actor details have been stored successfully!";
        }

        void UpdateActorList(Actor actor)
        {
            this._actors.Add(actor);
        }

        public int GetTotalActors()
        {
            return this._totalActors;
        }

        public List<Actor> GetActors()
        {
            if (this._totalActors == 0)
            {
                return null;
            }
            else
            {
                return this._actors;
            }
        }
    }
}
