﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ImbdLibrary
{
    public class Producer
    {
        private string _name;
        private int _id;
        private string _dateOfBirth;
        public void SetName(string name)
        {
            this._name = name;
        }

        public string GetName()
        {
            return this._name;
        }

        public void SetDOB(string dob)
        {
            this._dateOfBirth = dob;
        }

        public string GetDOB()
        {
            return this._dateOfBirth;
        }

        public int GetId()
        {
            return this._id;
        }

        public void SetId(int producerId)
        {
            this._id = producerId;
        }
    }
}
