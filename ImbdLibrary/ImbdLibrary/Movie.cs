﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ImbdLibrary
{
    public class Movie
    {
        private string _name;
        private string _yearOfRelease;
        private string _producer;
        private List<Actor> _actors;
        private string _plot;
        private int _id;

        public string GetName()
        {
            return this._name;
        }

        public void SetName(string name)
        {
            this._name = name;
        }

        public int GetId()
        {
            return this._id;
        }

        public void SetId(int id)
        {
            this._id = id;
        }

        public string GetPlot()
        {
            return this._plot;
        }

        public void SetPlot(string plot)
        {
            this._plot = plot;
        }

        public string GetYearOfRelease()
        {
            return this._yearOfRelease;
        }

        public void SetYearOfRelease(string year)
        {
            this._yearOfRelease = year;
        }

        public string GetProducer()
        {
            return this._producer;
        }

        public void SetProducer(string producerName)
        {
            this._producer = producerName;
        }

        public List<Actor> GetActors()
        {
            if(this._actors.Count == 0)
            {
                return null;
            }
            else
            {
                return this._actors;
            }
        }

        public void UpdateCast(Actor actor)
        {
            this._actors.Add(actor);
        }


       

        public Movie()
        {
            this._actors = new List<Actor>();
            this._name = "";
            this._producer = "";
            this._yearOfRelease = "";
        }

    }
}
