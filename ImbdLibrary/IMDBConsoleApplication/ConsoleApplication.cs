﻿using System;
using ImbdLibrary;
namespace IMDBConsoleApplication
{

    class ConsoleApplication
    {
        private readonly ImdbService _imdbService;
        private readonly ActorService _actorService;
        private readonly ProducerService _producerService;
        private int _choice;
        ConsoleApplication()
        {
            
            this._actorService = new ActorService();
            this._producerService = new ProducerService();
            this._imdbService = new ImdbService();
            this._choice = 0;
        }

        bool GetUserInput()
        {
            Console.WriteLine("\n-----------------IMDB-----------------");
            Console.WriteLine("1. List Movies");
            Console.WriteLine("2. Add Movie");
            Console.WriteLine("3. Add Actor");
            Console.WriteLine("4. Add Producer");
            Console.WriteLine("5. Delete Movie");
            Console.WriteLine("6. Exit");
            Console.Write("[SELECT] Please enter your choice: ");

            try
            {
                this._choice = Convert.ToInt32(Console.ReadLine());
                if (this._choice > 0 && this._choice < 7)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        static void Main(string[] args)
        {
            ConsoleApplication driver = new ConsoleApplication();

           
                while (true)
                {
                    var i_o = driver.GetUserInput();
                    if (!i_o)
                    {
                        continue;
                    }
                    switch (driver._choice)
                    {
                        case 1: driver._imdbService.DisplayMovies();                            
                        break;

                        case 2:
                            Console.WriteLine(driver._imdbService.AddMovie());
                            break;

                        case 3:
                            Console.WriteLine(driver._imdbService.AddActor());
                            break;

                        case 4:
                            Console.WriteLine(driver._imdbService.AddProducer());
                            break;

                        case 5:
                            Console.WriteLine(driver._imdbService.DeleteMovie());
                            break;

                        case 6:
                            Environment.Exit(0);
                            break;

                        default:
                            Console.WriteLine("Invalid Input!");
                            break;
                    }
                }

            }
        }
    }

